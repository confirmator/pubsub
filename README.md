# PubSub Server

## Requirements

- Docker, or
- Node.js

## Implementation Assumptions

- The pub/sub topic has no requirements as to what format is acceptable. This implementation assumes the standard Express server "word" format with a regex of `[A-Za-z0-9_]`.

- The sample publish request has a JSON `Content-Type` header while the subscribe one doesn't. This implementaiton assumes the header is required for both requests.

- This implementation assumes that subscriptions are not expected to retroactively handle published events. Only events published to a topic after a subscription are provided to clients. Events published before the topic is subscibed to are ignored.

- There appears to be no expectation/scope to unsubscribe from a topic one subscribed so this functionality is not implemented.

- The subscribe request body establishes a subscription to a topic for the enpoint URL specified in the request body. All clients connecting to that URL will receive the topic events the URL is subscribed to.

- The subsciption URLs should write a publish events' payload to log. This implementation assumes clients would be making simple `GET` requests to the subscription URLs.

- There appears to be no expectation/scope for an event queue for a given endpoint/topic to be flushed once a client receives the events (allowing for multiple clients and/or requests?) so this has not been implemented. Events will continue to accumulate indefinitely.

- This implementation will not scale outside of a single Node.js instance since subscriptions are stored in memory. For a scalable implementation we would need to store subscriptions in an central external cache (Redis, etc.)

## Using the Server

### Docker Run

```
./start-server.sh
```

### Node Run

```
npm run dev
```

### To subscribe an endpoint URL to a topic

```
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:8000/event" }' http://localhost:8000/subscribe/topic
```

### To publish to a topic

```
curl -X POST -H "Content-Type: application/json" -d '{ "message": "hello" }' http://localhost:8000/publish/topic
```

### To fetch subscribed events at an endpoint URL

```
curl -X GET -H "Content-Type: application/json" http://localhost:8000/event

```

## Running tests

```
npm run test
```

## Todo

- Setup environemnt configuration file

- OPTIONAL Setup unsubscribe for the subscription URLs
