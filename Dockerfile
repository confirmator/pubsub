FROM node:alpine

WORKDIR /opt/app

CMD ["npm", "run", "dev"]
