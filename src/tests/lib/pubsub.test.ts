// Module aliases would avoid all the path backtracking
import {
  subscribe,
  publish,
  getEndpointEvents,
  getSubscriptions,
  purgeSubscriptions,
  purgeEvents,
} from '../../lib/pubsub';

const endpoint = 'http://domain.test/endpoint';
const anotherEndoint = 'http://domain.test/another_endpoint';
const topic = 'topic';
const anotherTopic = 'another_topic';
const event = { message: 'hello' };

describe('subscribe()', () => {
  beforeEach(() => {
    purgeSubscriptions();
  });

  it('subscribes an endpoint to a topic', () => {
    subscribe(endpoint, topic);

    const actual = getSubscriptions();

    expect(actual).toStrictEqual([{ endpoint, topic }]);
  });

  it('subscribes an endpoint to a given topic only once', () => {
    subscribe(endpoint, topic);
    subscribe(endpoint, topic);

    const actual = getSubscriptions();

    expect(actual).toStrictEqual([{ endpoint, topic }]);
  });

  it('subscribes an endpoint to more than one topic', () => {
    subscribe(endpoint, topic);
    subscribe(endpoint, anotherTopic);

    const actual = getSubscriptions();

    expect(actual).toStrictEqual([
      { endpoint, topic },
      { endpoint, topic: anotherTopic },
    ]);
  });

  it('subscribes more than one endpoint to a topic', () => {
    subscribe(endpoint, topic);
    subscribe(anotherEndoint, topic);

    const actual = getSubscriptions();

    expect(actual).toStrictEqual([
      { endpoint, topic },
      { endpoint: anotherEndoint, topic },
    ]);
  });
});

describe('publish()', () => {
  beforeEach(() => {
    purgeSubscriptions();
    purgeEvents();
  });

  it('publishes an event only to the endpoints subscribed to a topic', () => {
    subscribe(endpoint, topic);
    subscribe(anotherEndoint, anotherTopic);

    publish(topic, event);

    const actualFromEndpoint = getEndpointEvents(endpoint);
    const actualFromAnotherEndpoint = getEndpointEvents(anotherEndoint);

    expect(actualFromEndpoint).toStrictEqual([{ topic, data: event }]);
    expect(actualFromAnotherEndpoint).toStrictEqual([]);
  });

  it('publishes an event to all endpoints subscribed to a topic', () => {
    purgeSubscriptions();
    purgeEvents();

    subscribe(endpoint, topic);
    subscribe(anotherEndoint, topic);

    publish(topic, event);

    const actualFromEndpoint = getEndpointEvents(endpoint);
    const actualFromAnotherEndpoint = getEndpointEvents(anotherEndoint);

    expect(actualFromEndpoint).toStrictEqual([{ topic, data: event }]);
    expect(actualFromAnotherEndpoint).toStrictEqual([{ topic, data: event }]);
  });
});
