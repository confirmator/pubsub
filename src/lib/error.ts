import { isCelebrate } from 'celebrate';

export const handleError = (err: any, req: any, res: any, next: any) => {
  const errorResponse = { error: err.message };
  if (isCelebrate(err)) {
    return res.status(400).json(errorResponse);
  }

  return res.status(500).json(errorResponse);
};
