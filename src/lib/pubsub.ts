import log from '../lib/logger';

interface ISubscription {
  endpoint: string;
  topic: string;
}

interface IEvent {
  endpoint: string;
  topic: string;
  data: unknown;
}

interface eventData {
  topic: string;
  data: unknown;
}

let subscriptions: ISubscription[] = [];

let events: IEvent[] = [];

const getSuscribedEndpoints = (topic: string): ISubscription[] => {
  return subscriptions.filter((subscription) => subscription.topic === topic);
};

const isEndpointSubscribed = (endpoint: string, topic: string) => {
  const subscirbers = getSuscribedEndpoints(topic);

  const endpointSubscription = subscirbers.filter(
    (subscriber) => subscriber.endpoint === endpoint
  );

  return endpointSubscription.length > 0;
};

export const subscribe = (endpoint: string, topic: string): void => {
  // Subscribe an endpoint to a topic only once
  if (isEndpointSubscribed(endpoint, topic)) {
    return;
  }

  subscriptions.push({ endpoint, topic });
  log.info(`PUBSUB: Subscribed /${endpoint} to '${topic}'`);
};

export const publish = (topic: string, data: unknown): void => {
  // Get all the endpoints subscribed to the topic
  const subscribers = getSuscribedEndpoints(topic);

  // Push the event data to all subscribers
  subscribers.map((subscriber) => {
    events.push({
      endpoint: subscriber.endpoint,
      topic: subscriber.topic,
      data,
    });

    log.info(
      `PUBSUB: Published data ${JSON.stringify(data)} to /${
        subscriber.endpoint
      }, topic '${subscriber.topic}'`
    );
  });
};

export const getEndpointEvents = (endpoint: string): eventData[] => {
  // Get all events this endpoint is subscribed to
  const endpointEvents = events.filter((event) => event.endpoint == endpoint);
  const response: eventData[] = [];
  endpointEvents.map((event) => {
    response.push({
      topic: event.topic,
      data: event.data,
    });
  });

  // Normally, we'd flush the event queue for the endpoint here
  // but it seems that is not the intent of the exercise

  log.info(
    `PUBSUB: Delivered /${endpoint} events: ${JSON.stringify(response)}`
  );

  return response;
};

export const getSubscriptions = () => {
  return subscriptions;
};

export const purgeSubscriptions = () => {
  subscriptions = [];
};

export const purgeEvents = () => {
  events = [];
};
