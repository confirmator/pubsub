import { createLogger, format, transports } from 'winston';
const { combine, timestamp, prettyPrint } = format;

// @todo The log path and filenames should be in an .env file
const logPath = 'log';

const logger = createLogger({
  level: 'info',
  format: combine(timestamp(), prettyPrint()),
  transports: [
    // Write all logs with level `error` and below to `error.log`
    new transports.File({
      filename: `${logPath}/error.log`,
      level: 'error',
    }),
    // Write all logs with level `info` and below to `combined.log`
    new transports.File({ filename: `${logPath}/combined.log` }),
  ],
});

// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new transports.Console({
      format: format.simple(),
    })
  );
}

export default logger;
