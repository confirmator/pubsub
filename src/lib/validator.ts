import { celebrate, Joi, Segments } from 'celebrate';

export const getSubscribeRequestValidator = () => {
  return celebrate({
    [Segments.BODY]: Joi.object().keys({
      url: Joi.string().uri(),
    }),
  });
};

export const getPublishRequestValidator = () => {
  return celebrate({
    [Segments.BODY]: Joi.object().required(),
  });
};

export const validateEndpoint = (endpoint: string) => {
  const schema = Joi.string().pattern(new RegExp('^[A-Za-z0-9_]{3,30}$'));
  return schema.validate(endpoint);
};
