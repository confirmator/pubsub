import { subscribe, publish, getEndpointEvents } from '../lib/pubsub';
import { validateEndpoint } from '../lib/validator';
import log from '../lib/logger';

export const subscribeAction = (req: any, res: any) => {
  // This assumes that the subscribe, publish and consume endpoints are all on the same domain
  // @todo The domain of the subcribe URL should instead be configured in .env
  // and properly validated
  const pathString = new URL(req.body.url).pathname.substr(1);

  const { error, value: endpoint } = validateEndpoint(pathString);

  if (error) {
    log.error(`CONTROLLER: ${error.message}`);
    return res.status(400).send(error.message);
  }

  subscribe(endpoint, req.params.topic);

  return res.status(200).send();
};

export const publishAction = (req: any, res: any) => {
  publish(req.params.topic, req.body);
  return res.status(200).send();
};

export const eventAction = (req: any, res: any) => {
  return res.status(200).send(getEndpointEvents(req.params.endpoint));
};
