import express from 'express';
import http from 'http';
import {
  getPublishRequestValidator,
  getSubscribeRequestValidator,
} from './lib/validator';
import { handleError } from './lib/error';
import { subscribeAction, publishAction, eventAction } from './app/controller';
import log from './lib/logger';

// @todo This should be in an .env file
const serverPort = process.env.PORT || 8000;

const app = express();
const server = http.createServer(app);

// Parse JSON requests
app.use(express.json());

// Setup routes
app.post('/subscribe/:topic', getSubscribeRequestValidator(), subscribeAction);
app.post('/publish/:topic', getPublishRequestValidator(), publishAction);
app.get('/:endpoint', eventAction);

// Apply error handling middleware
app.use((err: any, req: any, res: any, next: any) => {
  return handleError(err, req, res, next);
});

server.listen(serverPort, () => {
  log.info(`SERVER: Server listening on port ${serverPort}`);
});
